// ignore_for_file: use_build_context_synchronously

import 'dart:developer';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:tflite/tflite.dart';
import 'package:tflite_sample/main.dart';

class CaptureAndResult extends StatefulWidget {
  const CaptureAndResult({Key? key}) : super(key: key);

  @override
  State<CaptureAndResult> createState() => _CaptureAndResultState();
}

class _CaptureAndResultState extends State<CaptureAndResult> {
  CameraImage? cameraImage;
  CameraController? cameraController;

  @override
  void initState() {
    super.initState();
    loadCamera();
  }

  loadCamera() {
    cameraController = CameraController(cameras![0], ResolutionPreset.medium);
    cameraController!.initialize().then((value) {
      if (!mounted) {
        return;
      } else {
        setState(() {
          cameraController!.startImageStream((imageStream) {
            cameraImage = imageStream;
          });
        });
      }
    });
  }

  captureImage() async {
    if (cameraController!.value.isTakingPicture) {
      // Avoid capturing multiple images at once.
      return;
    }

    try {
      XFile image = await cameraController!.takePicture();
      // Navigate to the result screen with the captured image path.
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ResultScreen(imagePath: image.path),
        ),
      );
    } catch (e) {
      log('Error capturing image: $e');
    }
  }

  @override
  void dispose() {
    cameraController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Capture Image'),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child: SizedBox(
              height: MediaQuery.of(context).size.height * .7,
              width: MediaQuery.of(context).size.width,
              child: !cameraController!.value.isInitialized
                  ? Container()
                  : AspectRatio(
                      aspectRatio: cameraController!.value.aspectRatio,
                      child: CameraPreview(cameraController!),
                    ),
            ),
          ),
          ElevatedButton(
            onPressed: captureImage,
            child: const Text('Capture Image'),
          ),
        ],
      ),
    );
  }
}

class ResultScreen extends StatefulWidget {
  final String imagePath;

  const ResultScreen({super.key, required this.imagePath});

  @override
  State<ResultScreen> createState() => _ResultScreenState();
}

class _ResultScreenState extends State<ResultScreen> {
  String prediction = '';

  @override
  void initState() {
    super.initState();
    // Load the model and run the prediction when the result screen is initialized.
    loadModelAndRunPrediction();
  }

  loadModelAndRunPrediction() async {
    await Tflite.loadModel(
        model: 'assets/model.tflite', labels: 'assets/labels.txt');
    runModel();
  }

  runModel() async {
    try {
      final predictions = await Tflite.runModelOnImage(
        path: widget.imagePath,
        imageMean: 127.5,
        imageStd: 127.5,
        numResults: 2,
        threshold: 0.1,
        asynch: true,
      );
      if (predictions != null && predictions.isNotEmpty) {
        setState(() {
          prediction = predictions[0]['label'];
        });
      } else {
        log('No predictions found.');
      }
    } catch (e) {
      log('Error running model: $e');
    }
  }

  @override
  void dispose() {
    Tflite.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Result'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.file(File(widget.imagePath)),
            const SizedBox(height: 20),
            Text('Prediction: $prediction'),
          ],
        ),
      ),
    );
  }
}
